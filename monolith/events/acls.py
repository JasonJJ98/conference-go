import json
import requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city}, {state}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    d = json.loads(response.content)
    returns = {
        "picture_url": d["photos"][0]["src"]["original"]
    }
    try:
        return returns
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # geocode baseurl (before the ?)
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=params)
    d = response.json()
    try:
        lat = d[0]["lat"]
        lon = d[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "metric",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    d = response.json()
    try:
        return {
            "description": d["weather"][0]["description"],
            "temperature": f"{d['main']['temp']} °C",
        }
    except (KeyError, IndexError):
        return None
