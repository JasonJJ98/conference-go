# Generated by Django 4.0.3 on 2023-03-23 17:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0002_presentation_picture_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='presentation',
            name='picture_url',
        ),
    ]
